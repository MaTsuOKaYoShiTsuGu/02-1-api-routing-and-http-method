package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class QueryBookIntergrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Test
    void should_return_book_info_when_query_2_books() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 2"))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"));
    }

    @Test
    void should_return_book_info_when_query_23_books() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/23/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 23"))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"));
    }

    @Test
    void should_routing_definite_api() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"));
    }

    @Test
    void should_failed_when_uri_has_more_letter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/kayanonsuki"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_failed_when_uri_with_no_letter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/kayanon/sukidesu/"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_match_when_star_at_last() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("ok"));
    }

    @Test
    void should_match_when_star_inside_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("ok"));
    }

    @Test
    void should_failed_when_middle_segment_missing() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/before/after"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_match_when_two_star_around_segment() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/kayanon"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("ok"));
    }

    @Test
    void should_match_when_double_start_inside_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/kayanon/daisuki"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_match_when_use_regex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/kayanondaidaisuki"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
    
}
