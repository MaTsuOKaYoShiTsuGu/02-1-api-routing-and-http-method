package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QueryBook {
    @GetMapping("/api/users/{userID}/books/")
    String getUserInfo(@PathVariable long userID){
        return "The book for user "+String.valueOf(userID);
    }

    @GetMapping("/api/segments/good")
    String getMessage(){
        return "/api/segments/good";
    }

    @GetMapping("/api/segments/{segmentName}")
    String getMessage2(){
        return "/api/segments/{segmentName}";
    }

    @GetMapping("/api/kayanonsu?")
    String getWife(){return "kayanonsuki";}

    @GetMapping("/api/kayanon/sukidesu/?")
    String getWife2(){return "kayanonsuki";}

    @GetMapping("/api/wildcards/*")
    String getMessage3(){return "ok";}

    @GetMapping("/api/*/anything")
    String getMessage5(){return "ok";}

    @GetMapping("/api/wildcard/before/*/after")
    String getMessage6(){return "ok";}

    @GetMapping("/api/*yano*")
    String getMessage7(){return "ok";}

    @GetMapping("/api/kayanon/dai**suki")
    String getMessage8(){return "ok";}

    @GetMapping("/api/kayanonda[a-z]daisuki")
    String getMessage9(){return "ok";}
}
